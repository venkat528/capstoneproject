package com.hcl.cap.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.cap.dao.UserDetailRepository;
import com.hcl.cap.entity.UserDetail;

@Service
public class UserDetailService {
	
	@Autowired
	private UserDetailRepository userDetailRepository;
	

	public boolean insertUser(UserDetail userDetail) {
		if(!this.userDetailRepository.existsById(userDetail.getUserId())) {
			this.userDetailRepository.save(userDetail);
			return true;
		}
		return false;
	}


	public UserDetail getFindByEmail(String email) {
		return userDetailRepository.findByEmail(email);
	}
}

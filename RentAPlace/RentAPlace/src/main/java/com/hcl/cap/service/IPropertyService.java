package com.hcl.cap.service;

import java.util.List;

import com.hcl.cap.entity.BookedProperty;
import com.hcl.cap.entity.Property;

public interface IPropertyService {
	public Property getFindById(int id);

	public List<Property> getFindByLocation(String location);

	public List<Property> getFindByType(String type);

	public boolean insertBookedProperty(BookedProperty bookedProperty);

	public Property getToAddPropertyData(Property property);

	public boolean updateProperty(Property property);

	public List<BookedProperty> getpropertyStatusPendingAll();

	public boolean getPropertyStatusApproval(int id);

	public BookedProperty getFindByBookedId(int id);

}

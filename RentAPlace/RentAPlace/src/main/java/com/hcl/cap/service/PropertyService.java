package com.hcl.cap.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.cap.dao.BookedDataProperty;
import com.hcl.cap.dao.BookedPropertyRepository;
import com.hcl.cap.dao.PropertyRepository;
import com.hcl.cap.entity.BookedProperty;
import com.hcl.cap.entity.Property;


@Service
public class PropertyService implements IPropertyService{
	
	@Autowired
	private PropertyRepository propertyRepository;
	
	@Autowired
	private BookedPropertyRepository bookedPropertyRepository;
	
	@Autowired
	private BookedDataProperty bookedDataProperty;
	
	public Property getFindById(int id) {
		return propertyRepository.findById(id);
	}
	
	public List<Property> getFindByLocation(String location) {
		return propertyRepository.findByLocation(location);
	}
	
	public List<Property> getFindByType(String type) {
		List<Property> list=propertyRepository.findAll();
		List<Property> searched= new ArrayList<>();
		for(Property lists: list) {
			if(lists.getProperty_type().equals(type)) {
				searched.add(lists);
			}
		}
		return searched;
	}
	
	
	public boolean insertBookedProperty(BookedProperty bookedProperty) {
		if(this.bookedPropertyRepository.save(bookedProperty) != null) {
			return true;
		}
		return false;
	}
	
	
	public Property getToAddPropertyData(Property property) {
		return propertyRepository.save(property);
	}
	
	
	public boolean updateProperty(Property property) {
		if (this.propertyRepository.existsById(property.getProperty_id())) {
			this.propertyRepository.save(property);
			return true;
		}
		return false;
	}
	
	public List<BookedProperty> getpropertyStatusPendingAll() {
		List<BookedProperty> list=bookedDataProperty.findAll();
		System.out.println(list);
		List<BookedProperty> searched= new ArrayList<>();
		for(BookedProperty lists: list) {
			if(lists.getStatus().equals("Pending")) {
				searched.add(lists);
			}
		}
		System.out.println(searched);
		return searched;
	}
	
	public boolean getPropertyStatusApproval(int id) {
		BookedProperty booked_property=bookedPropertyRepository.findById(id);
		booked_property.setStatus("Approved");
		bookedPropertyRepository.save(booked_property);
		System.out.println(booked_property);
		return true;
	}
	
	public BookedProperty getFindByBookedId(int id) {
		return bookedPropertyRepository.findById(id);
	}
	
}

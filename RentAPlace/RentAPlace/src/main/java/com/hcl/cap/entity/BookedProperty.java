package com.hcl.cap.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class BookedProperty {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int booked_id;
	private String user_name;
	private String user_email;
	private String user_phone;
	private String room_type;
	private String room_name;
	private String date_from;
	private String due_date;
	private int no_of_nights;
	private int no_of_rooms;
	private int subtotal;
	private int tax;
	private int total;
	private String property_address;
	private String property_phone;
	private String property_email;
	private String status;
	
	
	
	
	
}


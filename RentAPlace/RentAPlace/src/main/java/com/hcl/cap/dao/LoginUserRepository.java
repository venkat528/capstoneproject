package com.hcl.cap.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.cap.entity.LoginUser;

@Repository
public interface LoginUserRepository extends JpaRepository<LoginUser, String> {

}

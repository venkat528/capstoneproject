package com.hcl.cap.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.cap.entity.Property;

@Repository
public interface PropertyRepository extends JpaRepository<Property, Integer>{
	
	List<Property> findAll();
	
	Property findById(int property_id);
	
	List<Property> findByLocation(String location);
	
}

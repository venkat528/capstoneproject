package com.hcl.cap.service;

import java.util.List;

import com.hcl.cap.entity.AdminDetail;

public interface IAdminDetailService {
	
	public List<AdminDetail> getAllAdmin(); 

}

package com.hcl.cap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Property {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int property_id;
	private String property_name;
	private String property_type;
	private String location;
	private String max_guest;
	private String address;
	private int rooms;
	private double room_size;
	private String property_line_content;
	@Column(length = 500)
	private String more_info;
	private int beds;
	private int price;
	private float ratings;
	private String image1;
	

	
	
}

package com.hcl.cap.service;

import com.hcl.cap.entity.LoginUser;

public interface ILoginUserService {
	
	public boolean insertUser(LoginUser loginUser);
	
	public boolean insertAdmin(LoginUser loginAdmin);

}

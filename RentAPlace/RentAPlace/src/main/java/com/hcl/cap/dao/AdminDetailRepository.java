package com.hcl.cap.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.cap.entity.AdminDetail;

@Repository
public interface AdminDetailRepository extends JpaRepository<AdminDetail, Integer> {

}

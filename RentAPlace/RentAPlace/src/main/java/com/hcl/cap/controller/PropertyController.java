package com.hcl.cap.controller;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.cap.RentAPlaceMicroserviceApplication;
import com.hcl.cap.dao.BookedDataProperty;
import com.hcl.cap.dao.BookedPropertyRepository;
import com.hcl.cap.dao.PropertyRepository;
import com.hcl.cap.entity.BookPropertyData;
import com.hcl.cap.entity.BookedProperty;
import com.hcl.cap.entity.Property;
import com.hcl.cap.entity.UserDetail;
import com.hcl.cap.service.EmailSenderService;
import com.hcl.cap.service.PropertyService;
import com.hcl.cap.service.UserDetailService;

@RestController
@CrossOrigin
public class PropertyController {

	@Autowired
	private PropertyRepository propertyRepository;
	
	@Autowired
	private PropertyService propertyService;
	
	@Autowired
	private UserDetailService userDetailService;
	
	@Autowired
	private BookedDataProperty bookedDataProperty;
	
	@Autowired
	private BookedPropertyRepository bookedPropertyRepository;
	
	@Autowired
	private EmailSenderService service;
	
	
	//Property CRUD Operations
	
	@GetMapping("/getRooms")
	public List<Property> getAllProperty() {
		return (List<Property>) propertyRepository.findAll();
	}

	@PostMapping("/addproperty")
	public Property getToAddProperty(@RequestBody Property property) {
		System.out.println("New Property Added");
		return propertyService.getToAddPropertyData(property);
	}
	
	
	@DeleteMapping("/deleteProperty/{id}")
	public boolean getDetetePropertyById(@PathVariable int id) {
		System.out.println(id);
		if (propertyRepository.findById(id) != null) {
			Property property= propertyRepository.findById(id);
			propertyRepository.delete(property);
			System.out.println("Property deleted");
			return true;
		}
		else {
			return false;
		}
		
	}

	@PostMapping("/editProperty")
	public boolean getEditProperty(@RequestBody Property property) {
		if(propertyService.updateProperty(property)) {
			System.out.println("Property Updated");
			System.out.println(property);
			return true;
		}
		else {
			return false;
		}
	}
	
	
	@GetMapping("/getBookedData")
	public List<BookedProperty> getAllBookedData() {
		return (List<BookedProperty>) bookedDataProperty.findAll();
	}
	
	
	@GetMapping("/propertyid/{id}")
	public Property getPropertyById(@PathVariable int id){
		return propertyService.getFindById(id);
	}
	
	@GetMapping("/prerpertyLocation/{location}")
	public List<Property> getPropertyByLocation(@PathVariable String location){	
		return propertyService.getFindByLocation(location);
	}
	
	
	@GetMapping("/prerpertyType/{type}")
	public List<Property> getPropertyByType(@PathVariable String type){	
		return propertyService.getFindByType(type);
	}
	
	
	@PostMapping("/bookProperty/{email}/{id}")
	public BookedProperty getStoreBookedProperty(@PathVariable int id,@PathVariable String email,@RequestBody BookPropertyData bookPropertyData) {
		System.out.println(id+"---"+email);
		Property property=propertyService.getFindById(id);
		UserDetail userDetail=userDetailService.getFindByEmail(email);
		System.out.println(property);
		System.out.println(userDetail);
		BookedProperty bookedProperty=new BookedProperty();
		bookedProperty.setUser_name(userDetail.getFirstname() +" "+userDetail.getLastname());
		bookedProperty.setUser_email(userDetail.getEmail());
		bookedProperty.setUser_phone(userDetail.getPhone());
		bookedProperty.setRoom_type(property.getProperty_type());
		bookedProperty.setRoom_name(property.getProperty_name());
		bookedProperty.setDate_from(bookPropertyData.getDate_from());
		bookedProperty.setDue_date(bookPropertyData.getDue_date());
		bookedProperty.setNo_of_nights(bookPropertyData.getNo_of_nights());
		bookedProperty.setNo_of_rooms(bookPropertyData.getNo_of_rooms());
		bookedProperty.setSubtotal(bookPropertyData.getSubtotal());
		bookedProperty.setTax(bookPropertyData.getTax());
		bookedProperty.setTotal(bookPropertyData.getTotal());
		bookedProperty.setProperty_address(property.getLocation());
		bookedProperty.setProperty_phone("");
		bookedProperty.setProperty_email("");
		bookedProperty.setStatus("Pending");
		
		propertyService.insertBookedProperty(bookedProperty);
		System.out.println(bookedProperty);
		System.out.println(bookedProperty.getBooked_id());
		return bookedProperty;
	}
	
	@GetMapping("/prorpertyStatusPendingAll")
	public List<BookedProperty> getprorpertyStatusPendingAll(){
		return propertyService.getpropertyStatusPendingAll();
	}
	
	@PostMapping("/prorpertyStatusApproval")
	public boolean getProrpertyStatusApproval(@RequestBody int id) {
		BookedProperty booked_data= bookedPropertyRepository.findById(id);
		service.emailForOwnerToUser(booked_data.getUser_email(), "Hello Sir,\n Your Property Booking Confirmed. \n\nThank You\n -Team Bookings.","Booking Confirmation Approved");
		return propertyService.getPropertyStatusApproval(id);
	}
	
	
	@EventListener(RentAPlaceMicroserviceApplication.class)
	@PostMapping("/bookedPropertyId")
	public BookedProperty getPropertyByBookedId(@RequestBody int id) throws MessagingException {
		BookedProperty booked_data= bookedPropertyRepository.findById(id);
		service.emailForUsertoOwner(booked_data.getUser_email(),booked_data.getUser_email(),
				"New booking Confirmation\nProperty Name :"+booked_data.getRoom_name()+"\n\nName :"+booked_data.getUser_name()+"\nPhone :"+booked_data.getUser_phone()
				+"\n\n     PRICE     \n--------------------------\nSubtotal :"+booked_data.getSubtotal()+"\nTax     :"+booked_data.getTax()+
				"\n\n--------------------------\n\n TOTAL   :"+booked_data.getTotal()+"\n\n Thank You.\n -Team Bookings",
				"Property Booking Confirmation From "+booked_data.getDate_from()+" - "+booked_data.getDue_date());
		return propertyService.getFindByBookedId(id);
	}
	
	


	
}

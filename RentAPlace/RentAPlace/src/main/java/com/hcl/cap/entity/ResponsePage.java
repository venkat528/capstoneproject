package com.hcl.cap.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor


public class ResponsePage {

	
	private Messages message;
	private String description;

	
}

package com.hcl.cap.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.cap.entity.BookedProperty;

@Repository
public interface BookedPropertyRepository extends JpaRepository<BookedProperty, Integer>{

	BookedProperty findById(int booked_id);
}

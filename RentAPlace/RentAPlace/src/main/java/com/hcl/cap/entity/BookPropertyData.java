package com.hcl.cap.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class BookPropertyData {
		
	private String date_from;
	private String due_date;
	private int no_of_nights;
	private int no_of_rooms;
	private int subtotal;
	private int tax;
	private int total;
	
	
	
	
	
}

package com.hcl.cap.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.cap.dao.AdminDetailRepository;
import com.hcl.cap.entity.AdminDetail;

@Service
public class AdminDetailService  implements IAdminDetailService {
	
	@Autowired
	private AdminDetailRepository adminDetailRepository;
	
	
	public List<AdminDetail> getAllAdmin() {
		List<AdminDetail> adminList=new ArrayList<AdminDetail>();
		this.adminDetailRepository.findAll().forEach(admin->adminList.add(admin));
		return adminList;
	}
	

}

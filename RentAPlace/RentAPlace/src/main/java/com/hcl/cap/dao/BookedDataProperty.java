package com.hcl.cap.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.cap.entity.BookedProperty;

@Repository
public interface BookedDataProperty extends JpaRepository<BookedProperty, Integer>{
	
	List<BookedProperty> findAll();
}

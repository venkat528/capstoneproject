import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AdminDetail } from '../model/admindetail';
import { Message } from '../model/messages';
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url: string = 'http://localhost:8080/';

  constructor(private router: Router, private http: HttpClient) { }

  isLoggedIn() {
    return !!localStorage.getItem('email');
  }

  checkLogin(user: AdminDetail): Observable<Message> {
    console.log(user);
    return this.http.post<Message>(this.url + 'adminAuthenticate', user);
  }
}

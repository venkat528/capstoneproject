import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private us:UserService ) { }
  intercept(req: { clone: (arg0: { setHeaders: { Authorization: string; }; }) => any; }, next: { handle: (arg0: any) => any; })
  {
    console.log('intercept req');
    let token = this.us.getToken();
    console.log(token)
    
    let tokenreq = req.clone({
      setHeaders:{
        Authorization:`${this.us.getToken()}`
      }
    });
  
    return next.handle(tokenreq);
  }

 
}

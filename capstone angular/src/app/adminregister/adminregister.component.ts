import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { adminregister } from '../model/adminregistration';
import { Message } from '../model/messages';
import { AdminregisterService } from '../service/adminregister.service';
@Component({
  selector: 'app-adminregister',
  templateUrl: './adminregister.component.html',
  styleUrls: ['./adminregister.component.css']
})
export class AdminregisterComponent implements OnInit {
  admin: adminregister;
  isError: boolean = false;
  errMessage: string = '';

  constructor(private us:AdminregisterService, private router: Router) {
    this.admin = { 'firstname': '', 'lastname': '', 'email': '', 'phone': '', 'address': '', 'password': '', 'confirm_password': '' };
  }

  ngOnInit(): void {
  }

  adminregister() {
    let that = this;
    this.us.adminRegister(this.admin)
      .subscribe({
        next(data: Message) {
          console.log(data.message);
          that.router.navigate(['adminlogin']);
        },
        error(data) {
          console.log('error call');
          console.log(data.error);
          that.isError = true;
          that.errMessage = data.error.description;
          console.log(that.errMessage);
        }
      });

    }

}
